const express = require("express");
const fs = require("fs").promises;
const globby = require("globby");
const path = require("path");
const puppeteer = require("puppeteer");
const replaceExt = require("replace-ext");


async function main() {
    const server = await startServer(3000);
    const browser = await startBrowser();

    await fs.mkdir("build", { recursive: true });

    for (const file of await globby("src/*.html")) {
        const fileName = path.basename(file);
        const outputFileName = replaceExt(fileName, ".pdf");

        await printToPdf(
            browser,
            `http://localhost:3000/${fileName}`,
            `build/${outputFileName}`,
        );
    }

    await server.close();
    await browser.close();
}

async function startServer(port) {
    const app = express();
    app.use(express.static(__dirname + "/src"));

    return new Promise((resolve, reject) => {
        const server = app.listen(port, (error) => {
            if (error) {
                return reject(error);
            }
        });

        return resolve(server);
    })
}

async function startBrowser() {
    const browser = await puppeteer.launch({
        args: [
            "--headless",
            "--no-sandbox",
            "--disable-gpu",
            "--disable-dev-shm-usage",
        ],
    });

    return browser;
}

async function printToPdf(browser, url, outputPath) {
    const page = await browser.newPage();
    await page.goto(url, {
        waitUntil: "networkidle0",
    });
    await page.pdf({
        path: outputPath,
        printBackground: true,
        format: "A4",
    });
}

main();
