const emojiSet = document.currentScript.getAttribute("data-emoji-set");

twemoji.parse(document.body, {
	base: "/js/twemoji-13.0.1",
    folder: `/${emojiSet}`,
    ext: ".svg",
});
